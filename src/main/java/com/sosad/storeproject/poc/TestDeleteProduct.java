/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sosad.storeproject.poc;
import database.Database;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Product;

/**
 *
 * @author Bunny0_
 */
public class TestDeleteProduct {
    public static void main(String[] args) {
        Connection c = null;
        Database db = Database.getInstance();
        c = db.getConnection();
        
        try{
            String updateQuery = "DELETE FROM product WHERE id=?";
            Product product = new Product(6,"KafaeYen",50);
            PreparedStatement statement = c.prepareStatement(updateQuery);
            statement.setInt(1, product.getId());
            int row = statement.executeUpdate();
            System.out.println("Affect row " + row);
        }catch(SQLException ex){
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE,null,ex);
        }
          db.close();
    }

}
